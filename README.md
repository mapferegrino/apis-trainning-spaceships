# README #

Este repositorio contiene un API de entrenamiento basada en el spec de OpenAPI
en su versión 3 y el propósito es puramente didáctico. 

### Para utilizarlo ###

* Clonar o realizar un fork del repositorio original
* Abrirlo con tu editor favorito de texto, si tiene extensión para openAPI mejor
* Puedes usar tmabién las herramientas que provee swagger para interpretar el código
